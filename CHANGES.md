### lab2 &rarr; lab1

- **Add directory spark** with 3 scripts respective to DAG tasks from lab1 which must be launch with [SparkSession](https://spark.apache.org/docs/latest/api/python/reference/pyspark.sql/api/pyspark.sql.SparkSession.html).
- **Change Dockerfile**: add installation of spark dependencies and copying of local spark directory to Airflow workdir
- **Change Docker-compose.yaml**: add 2 services connected with Spark (spark-master, spark-worker), describe dependencies of services for sequential running (postgres → spark-master → spark-worker → airflow-init → airflow)
- **Add Monitoring section to README**: description how to monitor state of Spark applications via Spark UI

### lab3 &rarr; lab2

- **Add tests**: validation of environment variables
- **Add .gitlab-ci.yml**: now it is possible to run app as a pipeline with three stages: test, build, deploy. 
