# NewsTranslator

NewsTranslator is a service for fetching and translation of [cnbc](https://www.cnbc.com/) news from English to other languages. For its purpose service uses Text-To-Text Transfer Transformer (T5) model [t5-small](https://huggingface.co/t5-small).

By default NewsTranslator fetches and translates Top 30 newest finance news. Default language for translation is French.

Publication date, translated title ans summary of news are saved to file **translated_news.csv** in directory **data**.


## Deploy

Build docker image from Dockerfile:

```bash
docker build . -t apache/airflow:custom
```
Run service`s containers (airflow, postgres) with docker-compose:

```bash
docker-compose up -d
```
Go to [localhost](http://localhost:8080/), login and password are default (login:airflow, password:airflow) and run dag.
## Usage

### News field

By default NewsTranslator fetches Top 30 newest finance news. News from other fields is available too. You can choose [rss feed](https://www.cnbc.com/rss-feeds/) which you need. To access other rss feed **before building docker image** you should change value of environment variable `CNBC_URL`, notably rss feed id:

```python
CNBC_URL = "https://www.cnbc.com/id/19746125/device/rss/rss.xml"
```

For example, to fetch and translate politics news, value of `CNBC_URL` must be:

```python
CNBC_URL = "https://www.cnbc.com/id/10000113/device/rss/rss.xml"
```
### Language of translation

By default NewsTranslator translates to French. Also German and Romanian are available. To access other language **before building docker image** you should change value of environment variable `LANGUAGE_TO`:

```python
LANGUAGE_TO = "French"
```
For example, to translate to German, value of `LANGUAGE_TO` must be:

```python
LANGUAGE_TO = "German"
```

## Monitoring

Spark is engaged in running Airflow DAG tasks. For monitoring state of Spark applications and workers [Spark UI](http://localhost:4040) should be used.


