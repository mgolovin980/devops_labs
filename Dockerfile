FROM apache/airflow:2.7.1

WORKDIR /opt/airflow
COPY ./requirements.txt ./requirements.txt

USER root 
RUN apt update && apt -y install procps default-jre 

USER airflow
RUN pip3 install apache-airflow-providers-apache-spark
RUN pip3 install --no-cache-dir --upgrade -r requirements.txt

COPY ./.env ./.env
COPY ./models ./models
COPY ./dags ./dags
COPY ./spark ./spark
