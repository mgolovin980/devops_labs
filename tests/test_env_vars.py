def test_language_to(language_to):
    assert language_to in ["German", "French", "Romanian"]


def test_cnbc_url(cnbc_url):
    assert "cnbc" in cnbc_url
    assert cnbc_url.split(".")[-1] == "xml"


def test_news_path(news_path):
    assert news_path.split(".")[-1] == "csv"
