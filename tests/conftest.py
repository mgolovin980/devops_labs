from dotenv import load_dotenv
import os
import pytest

load_dotenv()


@pytest.fixture
def language_to():
    return os.getenv("LANGUAGE_TO")


@pytest.fixture
def cnbc_url():
    return os.getenv("CNBC_URL")


@pytest.fixture(params=["NEWS_PATH", "TRANSLATED_NEWS_PATH"])
def news_path(request):
    yield os.getenv(request.param)
