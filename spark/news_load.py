import logging
import feedparser as fp
import pandas as pd
import html
from pyspark import SparkConf
from pyspark.sql import SparkSession
import os
import json

APP_NAME = os.getenv("APP_NAME")
SPARK_MASTER_URL = os.getenv("SPARK_MASTER_URL")

CNBC_URL = os.getenv("CNBC_URL")
COLUMNS_TO_SAVE = json.loads(os.getenv("COLUMNS_TO_SAVE"))
NEWS_PATH = os.getenv("NEWS_PATH")

logging.basicConfig(level=logging.INFO)


def news_load(feed_url: str, columns_to_save: list, news_path: str) -> None:
    logging.info("Fetching news from the cnbc.com starts")
    news_feed = fp.parse(feed_url)
    logging.info("News is fetched")
    df = pd.DataFrame(news_feed.entries)[columns_to_save]
    df["published"] = pd.to_datetime(df["published"])
    df["title"] = df["title"].map(html.unescape)
    df["summary"] = df["summary"].map(html.unescape)
    logging.info("Saving news starts")
    df.to_csv(news_path, sep="\t", index=False)
    logging.info(f"News is saved to {news_path}")


conf = SparkConf().setAppName(APP_NAME).setMaster(SPARK_MASTER_URL)
spark = SparkSession.builder.config(conf=conf).getOrCreate()
news_load(CNBC_URL, COLUMNS_TO_SAVE, NEWS_PATH)
