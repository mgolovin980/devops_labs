from transformers import AutoTokenizer, AutoModelForSeq2SeqLM
import pandas as pd
import logging
import os
from pyspark import SparkConf
from pyspark.sql import SparkSession

APP_NAME = os.getenv("APP_NAME")
SPARK_MASTER_URL = os.getenv("SPARK_MASTER_URL")

TOKENIZER_NAME = os.getenv("TOKENIZER_NAME")
TRANSLATOR_NAME = os.getenv("TRANSLATOR_NAME")
NEWS_PATH = os.getenv("NEWS_PATH")
TRANSLATED_NEWS_PATH = os.getenv("TRANSLATED_NEWS_PATH")

logging.basicConfig(level=logging.INFO)


def news_translate(
    tokenizer_name: str,
    translator_name: str,
    news_path: str,
    translated_news_path: str,
) -> None:
    logging.info("Initialization of tokenizer starts")
    tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)
    logging.info("Tokenizer is initialized")

    df = pd.read_csv(news_path, sep="\t")
    os.remove(news_path)

    title_inputs = tokenizer(
        df["title"].tolist(), padding=True, return_tensors="pt"
    ).input_ids
    summary_inputs = tokenizer(
        df["title"].tolist(), padding=True, return_tensors="pt"
    ).input_ids

    logging.info("Initialization of translator starts")
    model = AutoModelForSeq2SeqLM.from_pretrained(translator_name)
    logging.info("Translator is initialized")

    logging.info("Translation starts")
    title_outputs = model.generate(
        title_inputs, max_new_tokens=40, do_sample=True, top_k=30, top_p=0.95
    )
    summary_outputs = model.generate(
        summary_inputs, max_new_tokens=40, do_sample=True, top_k=30, top_p=0.95
    )

    df["title"] = tokenizer.batch_decode(title_outputs, skip_special_tokens=True)
    df["summary"] = tokenizer.batch_decode(summary_outputs, skip_special_tokens=True)
    logging.info("News is translated")
    df.to_csv(translated_news_path, sep="\t", mode="a", index=False)
    logging.info(f"Translated news is saved to {translated_news_path}")


conf = SparkConf().setAppName(APP_NAME).setMaster(SPARK_MASTER_URL)
spark = SparkSession.builder.config(conf=conf).getOrCreate()
news_translate(TOKENIZER_NAME, TRANSLATOR_NAME, NEWS_PATH, TRANSLATED_NEWS_PATH)
spark.stop()
