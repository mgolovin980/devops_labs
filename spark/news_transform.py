import logging
import pandas as pd
import os
import json
from pyspark import SparkConf
from pyspark.sql import SparkSession

APP_NAME = os.getenv("APP_NAME")
SPARK_MASTER_URL = os.getenv("SPARK_MASTER_URL")

NEWS_PATH = os.getenv("NEWS_PATH")
PREFIX = os.getenv("PREFIX")
COLUMNS_TO_TRANSLATE = json.loads(os.getenv("COLUMNS_TO_TRANSLATE"))

logging.basicConfig(level=logging.INFO)


def news_transform(news_path: str, columns_to_translate: list, prefix: str) -> None:
    df = pd.read_csv(news_path, sep="\t")
    logging.info("Transforming news data starts")
    df[columns_to_translate] = df[columns_to_translate].apply(lambda x: prefix + x)
    logging.info("News data is transformed")
    df.to_csv(news_path, sep="\t", index=False)


conf = SparkConf().setAppName(APP_NAME).setMaster(SPARK_MASTER_URL)
spark = SparkSession.builder.config(conf=conf).getOrCreate()
news_transform(NEWS_PATH, COLUMNS_TO_TRANSLATE, PREFIX)
