from airflow.utils.dates import days_ago
from airflow.models import DAG
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator
from dotenv import load_dotenv

load_dotenv()

dag_cnbc_news = DAG(
    "cnbc_news", start_date=days_ago(0), schedule="@daily", catchup=False
)

news_load = SparkSubmitOperator(
    task_id="news_load",
    application="/opt/airflow/spark/news_load.py",
    name="news_load",
    conn_id="spark_local",
    dag=dag_cnbc_news,
)

news_transform = SparkSubmitOperator(
    task_id="news_transform",
    application="/opt/airflow/spark/news_transform.py",
    name="news_transform",
    conn_id="spark_local",
    dag=dag_cnbc_news,
)

news_translate = SparkSubmitOperator(
    task_id="news_translate",
    application="/opt/airflow/spark/news_translate.py",
    name="news_translate",
    conn_id="spark_local",
    dag=dag_cnbc_news,
)

news_load >> news_transform >> news_translate
